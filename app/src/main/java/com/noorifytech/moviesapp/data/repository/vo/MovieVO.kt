package com.noorifytech.moviesapp.data.repository.vo

data class MovieVO(
    val id: Int,
    val title: String,
    val imageUrl: String,
    val page: Int
)