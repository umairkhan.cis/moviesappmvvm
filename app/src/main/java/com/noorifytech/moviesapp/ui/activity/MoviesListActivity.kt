package com.noorifytech.moviesapp.ui.activity

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedList
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.noorifytech.moviesapp.R
import com.noorifytech.moviesapp.data.repository.vo.MovieVO
import com.noorifytech.moviesapp.data.repository.vo.NetworkStatus
import com.noorifytech.moviesapp.ui.adapter.MoviesPagedListAdapter
import com.noorifytech.moviesapp.ui.callback.MoviesListCellCallback
import com.noorifytech.moviesapp.ui.navigator.Navigator
import com.noorifytech.moviesapp.ui.viewmodel.MoviesListViewModel
import com.noorifytech.moviesapp.ui.viewmodel.factory.MoviesListViewModelFactory
import kotlinx.android.synthetic.main.activity_movies_list.*

class MoviesListActivity : AppCompatActivity(), MoviesListCellCallback {

    private lateinit var moviesListAdapter: MoviesPagedListAdapter
    private lateinit var viewModelFactory: MoviesListViewModelFactory
    private lateinit var viewModel: MoviesListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_movies_list)

        viewModelFactory = MoviesListViewModelFactory.create()
        viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(MoviesListViewModel::class.java)

        initRecyclerView()

        init()
    }

    override fun onMovieSelected(movieId: Int, position: Int) {
        Navigator.navigateToMovieDetailsScreen(this, movieId)
    }

    private fun init() {
        viewModel.getPopularMovies()
            ?.observe(this, Observer<PagedList<MovieVO>> {
                moviesListAdapter.submitList(it)
            })

        viewModel.getPopularMoviesNetworkStatus()
            .observe(this, Observer<NetworkStatus> { networkStatus ->
                when (networkStatus) {
                    NetworkStatus.LOADING -> Toast.makeText(
                        this,
                        NetworkStatus.LOADING.name,
                        Toast.LENGTH_SHORT
                    ).show()

                    NetworkStatus.SUCCESS -> Toast.makeText(
                        this,
                        NetworkStatus.SUCCESS.name,
                        Toast.LENGTH_SHORT
                    ).show()

                    NetworkStatus.FAILED -> Toast.makeText(
                        this,
                        NetworkStatus.FAILED.name,
                        Toast.LENGTH_SHORT
                    ).show()

                    else -> Toast.makeText(
                        this,
                        NetworkStatus.FAILED.name,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
    }

    private fun initRecyclerView() {
        moviesListRV.layoutManager = LinearLayoutManager(this)
        moviesListRV.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        moviesListAdapter = MoviesPagedListAdapter(this, this)
        moviesListRV.adapter = moviesListAdapter
    }
}