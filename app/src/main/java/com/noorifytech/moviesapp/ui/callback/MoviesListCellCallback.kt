package com.noorifytech.moviesapp.ui.callback

interface MoviesListCellCallback {
    fun onMovieSelected(movieId: Int, position: Int)
}